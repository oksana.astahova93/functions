

const getSum = (str1, str2) => {
  if (
    !['string', 'number'].includes(typeof str1) ||
    !['string', 'number'].includes(typeof str2)
  ) {
    return false;
  }
  const num1 = Number(str1);
  const num2 = Number(str2);

  if (isNaN(num1) || isNaN(num2)) {
    return false;
  }
  return `${num1 + num2}`;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const {posts, comments} = listOfPosts.reduce((accumulator, post) => {
    if (post.author === authorName) {
      accumulator.posts++
    }
    if (post.comments) {
      accumulator.comments += post.comments.filter(comment => comment.author === authorName).length;
    }
    return accumulator;
  }, {posts: 0, comments: 0})
  return `Post:${posts},comments:${comments}`;
};

const tickets = (people) => {
  let money = {
    25: 0,
    50: 0,
    100: 0
  };
  for (let person of people) {
    money[person]++;
    switch (Number(person)) {
      case 25:
        break;
      case 50:
        money[25]--;
        break;
      case 100:
        money[50] ? money[50]-- : money[25] -= 2;
        money[25]--;
        break;
      default:
    }
    if (money[25] < 0) {
      return 'NO';
    }
  }

  return 'YES';
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
